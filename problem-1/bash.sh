#!/bin/bash

# Find all .csv ext files in current directory, output to target
find ./ -name '*.csv' -exec cp -prv '{}' 'output-csv/' ';'

# Find all .txt ext files in current directory, output to target
find ./ -name '*.txt' -exec cp -prv '{}' 'output-txt/' ';'

# Install needed packages
sudo apt-get install zip gzip

# Zip it up
zip -r output-csv.zip output-csv

# Transferring thru ssh
scp output-csv.zip username@machine:/path/to/where/it/is/headed