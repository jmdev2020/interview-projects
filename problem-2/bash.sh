#!/bin/bash

# We need to install the AWS CLI on the machine -- it isn't shipped
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip

# Run the install
sudo ./aws/install

# Confirm installation
aws --version

# Find the task definition of the 'api' service within the 'ref' cluster

for awsecstasks in $(aws ecs describe-services --services api --cluster ref)
do 
    echo $awsecstasks
done 

aws ecs describe-services --services api --cluster ref