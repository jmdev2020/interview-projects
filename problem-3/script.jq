def transformer:
  if length == 0 then []
  else [{filename: .[0], description: .[1]}] + (.[2:] | transformer)
  end;

inputs
| {region: .[0], region_code: .[1], period: .[2], geo_area: .[3], students_trained: .[4] } 
+ {transformer: (.[5:] | transformer) }