#!/bin/bash

# Download, save file
wget -O input.csv "https://dashboard.healthit.gov/api/open-api.php?source=workforce-progr
ams-trained.csv"

jq -R 'split(\",\")' input.csv | jq -f script.jq
